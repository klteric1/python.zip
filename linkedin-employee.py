from selenium import webdriver
import selenium
import time

from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

import sys
import requests
from bs4 import BeautifulSoup

import os
import csv
import datetime

print("Application starting...")
chrome_path = "/Users/Hung_Hsi_Huang/Desktop/chromedriver_installer-0.0.6/build/scripts-3.6/chromedriver1" #chromedriver.exe執行檔所存在的路徑
web = webdriver.Chrome(chrome_path)
url = 'https://www.linkedin.com/uas/login?session_redirect=/sales&fromSignIn=true&trk=navigator'

web.get('https://www.linkedin.com/uas/login?session_redirect=/sales&fromSignIn=true&trk=navigator')
print("Entering LinkedIn Website...")
previous_name = ""
number = 0
#web.set_window_position(0,0) #瀏覽器位置
#web.set_window_size(700,700) #瀏覽器大小
time.sleep(1)


word = web.find_element_by_xpath('//*[@id="username"]')
word.send_keys("miaoyen23@icloud.com")

word = web.find_element_by_xpath('//*[@id="password"]')
word.send_keys("Jane@0729")
print("Checking Authentication miaoyen23...")

time.sleep(2)
#點擊登入
web.find_element_by_xpath('//*[@id="app__container"]/main/div[2]/form/div[3]/button').click()
time.sleep(5)
print("Login success")

tag = web.page_source
soup = BeautifulSoup(tag, 'html.parser')

#點擊搜尋列表
web.find_element_by_xpath('//*[@id="ember33"]/span').click()
time.sleep(2)

#點擊account filter
web.find_element_by_xpath('/html/body/div[10]/div/div/div[2]/div/div[1]/div/div/button[2]').click()
time.sleep(2)

keywords1 = input("Enter keywords: ")
geography1 = input("Enter geography: ")
industry1 = input("Enter industry: ")

#找到enter keywords input位置
word = web.find_element_by_xpath('/html/body/div[10]/div/div/div[2]/div/div[2]/div/section[1]/ul/li[1]/div/div/input')
word.send_keys(keywords1)
time.sleep(2)
print("Keywords setting done")
#點擊滑鼠一下


#點擊geography
web.find_element_by_xpath('/html/body/div[10]/div/div/div[2]/div/div[2]/div/section[1]/ul/li[2]/div/div/div[1]/div/div/label').click()
time.sleep(2)

#找到add location位置
word = web.find_element_by_xpath('/html/body/div[10]/div/div/div[2]/div/div[2]/div/section[1]/ul/li[2]/div/div/div[3]/input')
word.send_keys(geography1)
time.sleep(2)

#點擊austrilia
web.find_element_by_xpath('/html/body/div[10]/div/div/div[2]/div/div[2]/div/section[1]/ul/li[2]/div/div/div[3]/ol/li[1]/button').click()
time.sleep(2)
print("Geography setting done")

#點擊Industry
web.find_element_by_xpath('/html/body/div[10]/div/div/div[2]/div/div[2]/div/section[1]/ul/li[3]/div/div/div/div/div/label').click()
time.sleep(2)

#找到add industry input位置
word = web.find_element_by_xpath('/html/body/div[10]/div/div/div[2]/div/div[2]/div/section[1]/ul/li[3]/div/div/div[3]/input')
word.send_keys(industry1)
time.sleep(2)

#點擊computer software
web.find_element_by_xpath('/html/body/div[10]/div/div/div[2]/div/div[2]/div/section[1]/ul/li[3]/div/div/div[3]/ol/li/button').click()
time.sleep(2)
print("Industry setting done")



#點擊搜尋
web.find_element_by_xpath('/html/body/div[10]/div/div/div[1]/div[2]/button/span').click()
time.sleep(10)
print("Searching done")

#ready to write file
datetime_dt = datetime.datetime.today()
datetime_str = datetime_dt.strftime('LinkenIn_Report%Y%m%d%H%M%S')
file_name = '/Users/Hung_Hsi_Huang/linkedin/'+datetime_str+'.csv'
writer = csv.writer (open(file_name,'a',encoding='MS950',errors='ignore',newline=''))
csvHeader = ['Company Name', 'Company Industry', 'Company Geography', 'Employee Name', 'Employee Title', 'Company page in LinkedIn']
writer.writerow(csvHeader)
print("CSV file has been built")

#按下第二個公司的view all employees
#/html/body/main/div[1]/div/section/div[1]/ol/li[2]/div[2]/div/div/div/article/section[2]/div/div/div[1]/a/span
def getdata(k):
	global previous_name
	global number
	i = int(k)
	#透過action 先找到要抓的物件位置
	print("開始抓公司名字的xpath位置")
	#找到公司名稱公司產業跟地點
	name = web.find_element_by_xpath("/html/body/main/div[1]/div/section/div[1]/ol/li["+k+"]/div[2]/div/div/div/article/section[1]/div[1]/dl/dt/a").text
	if i==1:
		previous_name = web.find_element_by_xpath("/html/body/main/div[1]/div/section/div[1]/ol/li[1]/div[2]/div/div/div/article/section[1]/div[1]/dl/dt/a").text

	print("找到公司名字text")
	industry = web.find_element_by_xpath("/html/body/main/div[1]/div/section/div[1]/ol/li["+k+"]/div[2]/div/div/div/article/section[1]/div[1]/dl/dd[3]/ul/li[1]").text
	print("找到產業名字text")
	location = web.find_element_by_xpath("/html/body/main/div[1]/div/section/div[1]/ol/li["+k+"]/div[2]/div/div/div/article/section[1]/div[1]/dl/dd[3]/ul/li[3]").text
	print("找到公司地址text")
	#按下公司的view all employees
	element = web.find_element_by_xpath("/html/body/main/div[1]/div/section/div[1]/ol/li["+k+"]/div[2]/div/div/div/article/section[2]/div/div/div[1]/a/span")
	print("找到公司人員連結")
	actions = ActionChains(web)
	actions.click()
	web.execute_script("arguments[0].click();", element)
	time.sleep(3)
	for m in range(1,26): #1-2 members
		n = str(m)
		try:
			#web.execute_script("window.scrollTo(0, document.body.scrollHeight)")
			#employee1 name and titles and url
			element = web.find_element_by_xpath("/html/body/main/div[1]/div/section/div[1]/div[1]/ol/li["+n+"]/div[2]/div/div/div/article/section[1]/div[1]/div/dl/dt/a")
			actions = ActionChains(web)
			actions.move_to_element(element).perform()
			time.sleep(0.2)
			employee_name = web.find_element_by_xpath("/html/body/main/div[1]/div/section/div[1]/div[1]/ol/li["+n+"]/div[2]/div/div/div/article/section[1]/div[1]/div/dl/dt/a").text
			employee_titles = web.find_element_by_xpath("/html/body/main/div[1]/div/section/div[1]/div[1]/ol/li["+n+"]/div[2]/div/div/div/article/section[1]/div[1]/div/dl/dd[2]/span[1]").text
			href = element.get_attribute('href')
			#print(href)
			writer.writerow((name, industry, location, employee_name, employee_titles, href))
			number = number+1
			print(number, " documents written into files")
		except selenium.common.exceptions.NoSuchElementException as e:
			print(e)
			print ("人員找不到",m)
			continue
		except Exception as e:
			print(e)
			continue
	#填完員工資料後返回上一頁到公司頁面
	web.back()
	time.sleep(3)




# write header to csv file
while True:
	articles = soup.find_all("body")
	try:
		for article in articles:
			for l in range(1,26): #1-25 company 每頁要抓公司的數目
				k = str(l)
				try:
					if l< 3:
						#web.execute_script("window.scrollTo(0, document.body.scrollHeight)")
						web.execute_script("window.scrollTo(0, 200)")
						time.sleep(0.2)
						getdata(k)
					elif l>=4 and l<=7 :
						web.execute_script("window.scrollTo(0, 700)")
						time.sleep(0.2)
						getdata(k)
					elif l>=8 and l<=9 :
						web.execute_script("window.scrollTo(0, 1400)")
						time.sleep(0.2)
						getdata(k)
					elif l==10 :   
						web.execute_script("window.scrollTo(0, 1640)")
						time.sleep(0.2)
						getdata(k)
					elif l==11 :  
						web.execute_script("window.scrollTo(0, 1840)")
						time.sleep(0.2)
						getdata(k)
					elif l>=12 and l<=13 :   
						web.execute_script("window.scrollTo(0, 2303)")
						time.sleep(0.2)
						getdata(k)
					elif l==14 :   
						web.execute_script("window.scrollTo(0, 3000)")
						time.sleep(0.2)
						getdata(k)
					elif l>=15 and l<=16 :  
						web.execute_script("window.scrollTo(0, 3003)")
						time.sleep(0.2)
						getdata(k)
					elif l>=17 and l<=18 :   
						web.execute_script("window.scrollTo(0, 3700)")
						time.sleep(0.2)
						getdata(k)
					elif l==19 :   
						web.execute_script("window.scrollTo(0, 3766)")
						time.sleep(0.2)
						getdata(k)
					elif l==20 :  
						web.execute_script("window.scrollTo(0, 4000)")
						time.sleep(0.2)
						getdata(k)
					elif l==21 :  
						web.execute_script("window.scrollTo(0, 4200)")
						time.sleep(0.2)
						getdata(k)
					elif l==22 :  
						web.execute_script("window.scrollTo(0, 4400)")
						time.sleep(0.2)
						getdata(k)
					else:
						web.execute_script("window.scrollTo(0, document.body.scrollHeight)")
						#web.execute_script("window.scrollTo(0, 3200)")
						time.sleep(0.2)
						getdata(k)
				except selenium.common.exceptions.NoSuchElementException as e:
					pass
					print (e)
					print ("公司找不到",l)
				except Exception as e:
					pass
					print (e)
					web.refresh()
			#點擊搜尋頁面下一頁
			actions = ActionChains(web)
			actions.click()
			element = web.find_element_by_xpath("/html/body/main/div[1]/div/section/div[2]/nav/button[2]")
			web.execute_script("arguments[0].click();", element)
			time.sleep(3)
			print('Entering next page')
			#換到下一頁時，要檢查是否到底頁面重複了，檢查方式為比對第一筆公司名稱
			pagecheck = web.find_element_by_xpath("/html/body/main/div[1]/div/section/div[1]/ol/li[1]/div[2]/div/div/div/article/section[1]/div[1]/dl/dt/a").text
			if previous_name == pagecheck :
				try:
					print("資料收集完畢")
					web.close()
				except Exception as e:
					print(e)
					sys.exit(1)
					web.close()
#	 			try:
#	 				web.find_element_by_xpath("/html/body/main/div[1]/div/section/div[2]/nav/button[2]")
#	 			except Exception as e:
#	 				print(e)
	 			

#	 			writer.writerow((web.find_element_by_xpath("/html/body/main/div[1]/div/section/div[1]/ol/li[1]/div[2]/div/div/div/article/section[1]/div[1]/dl/dt/a").text,
#	 							web.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[7]/td/table/tbody/tr["+j+"]"+"/td[2]").text,
#	 							web.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[7]/td/table/tbody/tr["+j+"]"+"/td[3]").text,
##	 							web.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[7]/td/table/tbody/tr["+j+"]"+"/td[5]").text,
#								web.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[7]/td/table/tbody/tr["+j+"]"+"/td[6]").text,
#								web.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[7]/td/table/tbody/tr["+j+"]"+"/td[7]").text,
#								web.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[7]/td/table/tbody/tr["+j+"]"+"/td[8]").text,
#								web.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[7]/td/table/tbody/tr["+j+"]"+"/td[9]").text,
#								web.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[7]/td/table/tbody/tr["+j+"]"+"/td[10]").text,
#								web.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[7]/td/table/tbody/tr["+j+"]"+"/td[12]").text,
#								))


		 #選下一頁  
#	except UnicodeEncodeError:
	except requests.exceptions.RequestException as e:
		print (e)
	except selenium.common.exceptions.NoSuchElementException as e:
		pass
		print (e)
		sys.exit(1)





#file.close()
time.sleep(2)
web.close()









